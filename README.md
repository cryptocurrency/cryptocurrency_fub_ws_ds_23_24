# Tic-Tac-Toe Smart Contract

This project implements a Tic-Tac-Toe game as a Solidity smart contract. It allows two players to play the classic game on the Ethereum blockchain.

## Features

- Two-player gameplay
- Automatic turn management
- Win detection
- Game state visualization
- Tie game detection

## Prerequisites

- Solidity ^0.8.22
- An Ethereum development environment (e.g., Truffle, Hardhat, or Remix)

## Contract Overview

The `TicTacToe` contract includes the following main functions:

- `constructor(address _player1, address _player2)`: Initializes the game with two players.
- `playRound(uint8 yvar, uint8 xvar)`: Allows a player to make a move.
- `checkWinner()`: Returns the address of the winner, if any.
- `isGameEnded()`: Checks if the game has ended.
- `printState()`: Returns a string representation of the current game board.

## How to Use

1. Deploy the contract, providing the addresses for Player 1 and Player 2.
2. Players take turns calling the `playRound` function with their desired move coordinates.
3. Use `printState` to view the current game board.
4. Check `isGameEnded` to see if the game is over, and `checkWinner` to determine the winner.


## License

This project is licensed under the MIT License. See the SPDX-License-Identifier at the top of the contract file for details.
